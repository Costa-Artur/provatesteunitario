﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bogus;
using Prova;

namespace Prova.Tests.ExchangeRateTests
{
    public class ExchangeRateTestsFixture : IClassFixture<ExchangeRateTestsFixture>
    {
        public ExchangeRate GenerateInvalidFactorForExchangeRate ()
        {
            var exchangeRate = new Faker<ExchangeRate>("pt_BR")
                .CustomInstantiator(f => new ExchangeRate(f.Date.RecentDateOnly(),
                0,
                f.Random.Guid(),
                f.Random.Guid(),
                f.Random.Guid()
                ));
            return exchangeRate;
        }

        public ExchangeRate GenerateInvalidTypeIdForExchangeRate()
        {
            var exchangeRate = new Faker<ExchangeRate>("pt_BR")
                .CustomInstantiator(f => new ExchangeRate(f.Date.RecentDateOnly(),
                f.Random.Decimal(),
                Guid.Empty,
                f.Random.Guid(),
                f.Random.Guid()
                ));
            return exchangeRate;
        }

        public ExchangeRate GenerateFactorWithMoreThan5DecimalsForExchangeRate()
        {
            var exchangeRate = new Faker<ExchangeRate>("pt_BR")
                .CustomInstantiator(f => new ExchangeRate(f.Date.RecentDateOnly(),
                0.123456789m,
                f.Random.Guid(),
                f.Random.Guid(),
                f.Random.Guid()
                ));
            return exchangeRate;
        }

    }
}
