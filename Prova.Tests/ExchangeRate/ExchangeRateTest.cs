using FluentAssertions;
using System.Numerics;

namespace Prova.Tests.ExchangeRateTests
{
    public class ExchangeRateTest : IClassFixture<ExchangeRateTestsFixture>
    {
        private ExchangeRateTestsFixture _fixture;
        public ExchangeRateTest(ExchangeRateTestsFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact(DisplayName = "Invalid Factor For ExchangeRate")]
        [Trait("Category", "ExchangeRate Entity Unit Tests")]
        public void IsValid_WhenFactorIssEqualToZero_ShouldReturnFalseAndHaveErrors()
        {
            //Arrange
            //var exchangeRate = _fixture.GenerateInvalidFactorForExchangeRate();

            //Act
            //Action act = () => _fixture.GenerateInvalidFactorForExchangeRate();
            var exception = Record.Exception(() => _fixture.GenerateInvalidFactorForExchangeRate());


            //Assert
            Assert.IsType<DomainException>(exception);
            Assert.Equal("The Factor cannot be zero", exception.Message);
        }

        [Fact(DisplayName = "Invalid TypeId For ExchangeRate")]
        [Trait("Category", "ExchangeRate Entity Unit Tests")]
        public void IsValid_WhenTypeIdIsEmpty_ShouldReturnFalseAndHaveErrors()
        {
            //Arrange
            //var exchangeRate = _fixture.GenerateInvalidTypeIdForExchangeRate();

            ////Act
            //Action act = () => _fixture.GenerateInvalidTypeIdForExchangeRate();
            var exception = Record.Exception(() => _fixture.GenerateInvalidTypeIdForExchangeRate());

            //Assert
            Assert.IsType<DomainException>(exception);
            Assert.Equal("The TypeId cannot be empty", exception.Message);
        }

        [Fact(DisplayName = "Factor With More than 5 Decimals For ExchangeRate")]
        [Trait("Category", "ExchangeRate Entity Unit Tests")]
        public void IsValid_WhenFactorHasMoreThan5Decimals_ShouldReturnTrueAndHaveNoErrors()
        {
            //Arrange
            //var exchangeRate = _fixture.GenerateInvalidTypeIdForExchangeRate();

            ////Act
            //Action act = () => _fixture.GenerateInvalidTypeIdForExchangeRate();
            var exchangeRate =  _fixture.GenerateFactorWithMoreThan5DecimalsForExchangeRate();

            //Assert
            Assert.Equal(0.12346m, exchangeRate.Factor);


        }
    }
}